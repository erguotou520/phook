package config

import (
	"flag"
	"github.com/BurntSushi/toml"
)

type tomlConfig struct {
	Host    string
	Port    int
	Webhook string
	Branch  string
	Name    string
	UUID    string `toml:"uuid"`
	Output  string
}

var (
	// Config global config
	Config tomlConfig
)

func init() {
	// 读取配置文件
	confFile := flag.String("c", "./config.toml", "config file")
	flag.Parse()
	Config = tomlConfig{
		Host:   "0.0.0.0",
		Port:   6789,
		Branch: "master",
		Output: "./",
	}
	if _, err := toml.DecodeFile(*confFile, &Config); err != nil {
		panic(err)
	}
	if Config.Port <= 1024 {
		Config.Port = 6789
	}
}
