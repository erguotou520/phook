package repository

import (
	"errors"
	"log"
	"os"
	"os/exec"
)

var (
	// GitPath global git exec command path
	GitPath string
)

// LocalRepository local repository
type LocalRepository struct {
	URL          string
	Name         string
	TargetBranch string
	OutputPath   string
}

func init() {
	var err error
	GitPath, err = exec.LookPath("git")
	if err != nil {
		log.Fatal("No git command, please install git first")
	}
}

// UpdateRepository clone or pull code
func (repo *LocalRepository) UpdateRepository() error {
	if stat, err := os.Stat(repo.OutputPath); err == nil && stat.IsDir() {
		// folder existed, then pull
		log.Print("Meets the configed branch, then pulling")
		return repo.PullRepository()
	}
	// folder not existed, then clone
	log.Print("Meets the configed branch, then cloning")
	return repo.CloneRepository()
}

// CloneRepository clone code
func (repo *LocalRepository) CloneRepository() error {
	log.Print("git", " clone -b "+repo.TargetBranch+" "+repo.URL+" "+repo.OutputPath)
	cmd := exec.Command(GitPath, "clone", "-b", repo.TargetBranch, repo.URL, repo.OutputPath)
	err := cmd.Run()
	if err != nil {
		return errors.New(repo.Name + " clone failed: " + err.Error())
	}
	log.Print(repo.Name + " clone done.")
	return nil
}

// PullRepository pull code
func (repo *LocalRepository) PullRepository() error {
	cmd := exec.Command(GitPath, "-C", repo.OutputPath, "fetch", "--all")
	if err := cmd.Run(); err != nil {
		return errors.New(repo.Name + " pull failed: " + err.Error())
	}
	cmd = exec.Command(GitPath, "-C", repo.OutputPath, "reset", "--hard", "origin/"+repo.TargetBranch)
	if err := cmd.Run(); err != nil {
		return errors.New(repo.Name + " pull failed: " + err.Error())
	}
	log.Print(repo.Name + " pull done.")
	return nil
}
