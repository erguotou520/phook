# Bitbucket部署脚本

基于`Bitbucket`的前端部署脚本

## 前提

- 使用`bitbucket-pipelines.yml`完成CI构建工作，并提交到指定的分支
  代码参考

  ```yml
  image: node:6.9.4
  pipelines:
    default:
      - step:
          caches:
            - node
          script: # Modify the commands below to build your repository.
            - apt-get update
            - npm install --loglevel warn
            - npm run build
            - cd dist
            - git config --global push.default matching
            - git config --global user.name "erguotou"
            - git config --global user.email "erguotou525@gmail.com"
            - git init
            - git add ./ -A
            - git commit -m "Auto build"
            - git push https://erguotou520:$APP_PASSWORD@bitbucket.org/erguotou520/repo.git HEAD:build --force
  ```

## 使用方法

先创建`config.toml`配置文件，文件格式如下

```toml
# 非必需 默认值为webhook里的FullName
name = "App名称"
# 非必需 默认值'0.0.0.0' 服务监听的地址
host = "0.0.0.0"
# 非必需 默认值6789 服务监听的端口，需大于1024
port = 6789 # 注意是数字
# 非必需 默认值'master' 要监听的分支，其它分支的推送信息将无视
branch = "build"
# 必需 默认值'' Bitbucket发送的webhook数据的Header中的X-Hook-UUID的值
uuid = "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxx"
# 非必需 默认值'./' 代码copy到的目录，需要指定项目目录
output = "path/to/clone"
# 非必需 默认值'' 可以指定在代码clone后向该地址发送一个post请求，可以结合例如`Slack`进行结果跟踪
webhook = "https://hooks.slack.com/services/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"
```

然后运行`go run phook.go -c path/to/config.toml`，如果`config.toml`在当前目录，可以直接运行`go run phook.go`。  
当然，你也可以使用编译好的脚本运行`phook.exe`

### 注意

请确保`output`的目录不存在，如果存在，需要初始化下目录

```bash
# 如果不是git仓库的话
git init
# 如果没有配置远程地址的话
git remote add origin git@bitbucket.org:user/repo.git
git fetch
git checkout branch
```

### 构建

GOOS=linux GOARCH=amd64 go build -o phook .
GOOS=darwin GOARCH=amd64 go build -o phook_macos_amd64 .
GOOS=windows GOARCH=amd64 go build -o phookwindows.exe .