package main

import (
	"log"

	"github.com/erguotou520/phook/config"
	"github.com/erguotou520/phook/repository"
	"github.com/erguotou520/phook/webhook"
	webhooks "gopkg.in/go-playground/webhooks.v3"
	"gopkg.in/go-playground/webhooks.v3/bitbucket"
	"strconv"
	"time"
)

func postHook(username string, str string) {
	if config.Config.Webhook != "" {
		wh := webhook.NewWebHook(config.Config.Webhook)
		wh.PostMessage(&webhook.WebHookPostPayload{Username: username, Text: str})
	}
}

// HandleBuildBranch webhook payload handler
func HandleBuildBranch(payload interface{}, header webhooks.Header) {
	pl := payload.(bitbucket.RepoPushPayload)
	log.Printf("Received wehbook payload, type: %s, name: %s\nCommitor: %s\nCommit message: %s",
		pl.Push.Changes[len(pl.Push.Changes)-1].New.Type,
		pl.Push.Changes[len(pl.Push.Changes)-1].New.Name,
		pl.Actor.DisplayName,
		pl.Push.Changes[len(pl.Push.Changes)-1].New.Target.Message,
	)
	if pl.Push.Changes[len(pl.Push.Changes)-1].New.Type == "branch" && pl.Push.Changes[len(pl.Push.Changes)-1].New.Name == config.Config.Branch {
		repo := &repository.LocalRepository{
			URL:          "git@bitbucket.org:" + pl.Repository.FullName + ".git",
			Name:         pl.Repository.FullName,
			TargetBranch: config.Config.Branch,
			OutputPath:   config.Config.Output,
		}
		notifyName := config.Config.Name
		if notifyName == "" {
			notifyName = pl.Repository.FullName
		}
		if err := repo.UpdateRepository(); err != nil {
			log.Print(err.Error())
			postHook(notifyName, err.Error()+":cry:")
		} else {
			postHook(notifyName, repo.Name+" updated at "+time.Now().Format("2006-01-02 15:04:05")+"!:smile:")
		}
	} else {
		log.Print("Not meets the configed branch")
	}
}

func main() {
	portStr := strconv.FormatInt(int64(config.Config.Port), 10)
	hook := bitbucket.New(&bitbucket.Config{UUID: config.Config.UUID})
	hook.RegisterEvents(HandleBuildBranch, bitbucket.RepoPushEvent)
	err := webhooks.Run(hook, config.Config.Host+":"+portStr, "/webhook")
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Server is listening at: %s", config.Config.Host+":"+portStr)
}
